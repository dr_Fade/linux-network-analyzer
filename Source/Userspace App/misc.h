#ifndef MISC_H
#define MISC_H

// here goes stuff that doesn't fit anywhere else :P

#include <QtCore>
#include <QApplication>
#include <signal.h>
#include "../LKM/netmon.h"

enum MAGIC_NUMBERS : qint64
{
    GEN_TABLE_SIZE = 15,
    ROUTING_TABLE_COLUMNS = 4,
    LINES_ON_CHART = 6,
    MAX_TIME_RANGE = 256,
    KB = 1024,
    MB = 1048576,
    GB = 10737418240
};

enum Packets
{
    TCP     = 0,
    UDP     = 3,
    ICMP    = 6,
    OTHER   = 9
};

enum Hook
{
    IN,
    OUT,
    FWD
};

enum HooksIOCTL
{
    IN_UP       = 1,
    IN_DOWN     = 2,
    OUT_UP      = 3,
    OUT_DOWN    = 4,
    FWD_UP      = 5,
    FWD_DOWN    = 6
};

struct routingTableItem
{
    QString srcIP,
            dstIP;
    quint64 totalAmount,
            totalSize;

    routingTableItem(const QString& _srcIP = "",
                     const QString& _dstIP = "",
                     const quint64 _tA = 0,
                     const quint64 _tS = 0):
    srcIP(_srcIP),
    dstIP(_dstIP),
    totalAmount(_tA),
    totalSize(_tS)
    {}
};

using infoVector = QVector<quint64>;
using routingInfoVector = QVector<routingTableItem>;

#endif // MISC_H
