#include "./UI/interfacemanager.h"

void term(int signum)
{
    qWarning() << signum << "recieved. Terminating!";
    QApplication::quit();
}

int main(int argc, char *argv[])
{
    qRegisterMetaType<infoVector>("infoVector");
    qRegisterMetaType<Hook>("Hooks");
    qRegisterMetaType<HooksIOCTL>("HooksIOCTL");

    struct sigaction action;
    memset(&action, 0, sizeof(struct sigaction));
    action.sa_handler = term;
    sigaction(SIGTERM, &action, NULL);

    QApplication a(argc, argv);
    interfaceManager w;
    w.show();

    return a.exec();
}
