#include "interfacemanager.h"
#include "ui_interfacemanager.h"
interfaceManager::interfaceManager(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::interfaceManager)
{
    ui->setupUi(this);
    m_data = new dataManager(this);

    //adding chart widget
    m_chartView = new QChartView(this);
    scaleChart();
    m_chartView->setChart(m_data->getChart());
    ui->tabChartView->layout()->addWidget(m_chartView);

    connect(ui->sliderTimeAxis, &QAbstractSlider::valueChanged,
            this, &interfaceManager::scaleChart);

    connect(m_data, &dataManager::tablesUpdated,
            this, &interfaceManager::updateTables);

    connect(ui->checkBoxIN, &QCheckBox::clicked,
            this, &interfaceManager::INswitched);

    connect(ui->checkBoxOUT, &QCheckBox::clicked,
            this, &interfaceManager::OUTswitched);

    connect(ui->checkBoxFWD, &QCheckBox::clicked,
            this, &interfaceManager::FWDswitched);

    connect(ui->checkBoxChartIN, &QCheckBox::clicked,
            this, &interfaceManager::switchLines);

    connect(ui->checkBoxChartOUT, &QCheckBox::clicked,
            this, &interfaceManager::switchLines);

    connect(ui->checkBoxChartFWD, &QCheckBox::clicked,
            this, &interfaceManager::switchLines);

    connect(ui->checkBoxChartINsize, &QCheckBox::clicked,
            this, &interfaceManager::switchLines);

    connect(ui->checkBoxChartOUTsize, &QCheckBox::clicked,
            this, &interfaceManager::switchLines);

    connect(ui->checkBoxChartFWDsize, &QCheckBox::clicked,
            this, &interfaceManager::switchLines);

    connect(ui->actionSave, &QAction::triggered,
            this, &interfaceManager::saveStats);

    connect(ui->actionLoad, &QAction::triggered,
            this, &interfaceManager::loadStats);

    connect(ui->routingTable->horizontalHeader(), &QHeaderView::sectionClicked,
            this, &interfaceManager::sortRoutingTable);

    //setting up general info table
    QStringList lst;
    lst << QString("IN") << QString("FWD") << QString("OUT");
    ui->generalTable->setHorizontalHeaderLabels(lst);
    lst.clear();
    lst << QString("TCP") << QString("UDP") << QString("ICMP") << QString("OTHER") << QString("Size");
    ui->generalTable->setVerticalHeaderLabels(lst);

    for (int i = 0; i < ui->generalTable->horizontalHeader()->count(); ++i)
        ui->generalTable->horizontalHeader()->setSectionResizeMode(i, QHeaderView::Stretch);
    for (int i = 0; i < ui->generalTable->verticalHeader()->count(); ++i)
        ui->generalTable->verticalHeader()->setSectionResizeMode(i, QHeaderView::Stretch);

    for(int i=0; i<5; i++)
        for(int j=0; j<3; j++)
            ui->generalTable->setItem(i,j, new QTableWidgetItem());

    //setting up routing table
    lst.clear();
    lst << QString("Source") << QString("Destination") << QString("Total amount") << QString("Total size");
    ui->routingTable->setHorizontalHeaderLabels(lst);

    for (int i = 0; i < ui->routingTable->horizontalHeader()->count(); ++i)
        ui->routingTable->horizontalHeader()->setSectionResizeMode(i, QHeaderView::Stretch);
    for (int i = 0; i < ui->routingTable->verticalHeader()->count(); ++i)
        ui->routingTable->verticalHeader()->setSectionResizeMode(i, QHeaderView::Stretch);
}

interfaceManager::~interfaceManager()
{
    delete ui;
}

void interfaceManager::scaleChart()
{
    m_data->scaleTimeAxis(ui->sliderTimeAxis->value());
}

void interfaceManager::updateTables(infoVector generalInfo, routingInfoVector routingInfo)
{
    // general info
    qint32 row, col;
    for(qint32 i=0; i<GEN_TABLE_SIZE - 3; i++)
    {
        row = i/3;
        col = i%3;
        ui->generalTable->item(row,col)->setText(QString::number(generalInfo[i]));
    }
    for(qint32 i=GEN_TABLE_SIZE - 3; i<GEN_TABLE_SIZE; i++)
    {
        row = i/3;
        col = i%3;
        if(generalInfo[i] > GB)
            ui->generalTable->item(row,col)->setText(QString::number(generalInfo[i]/GB) + " GB");
        if(generalInfo[i] > MB)
            ui->generalTable->item(row,col)->setText(QString::number(generalInfo[i]/MB) + " MB");
        else if(generalInfo[i] > KB)
            ui->generalTable->item(row,col)->setText(QString::number(generalInfo[i]/KB) + " KB");
        else
            ui->generalTable->item(row,col)->setText(QString::number(generalInfo[i]) + " B");
    }

    // routing info

    // add more rows if needed
    if(routingInfo.size() > ui->routingTable->rowCount())
    {
        for(qint32 i=ui->routingTable->rowCount(); i<routingInfo.size(); i++)
        {
            ui->routingTable->insertRow(i);
            for(int j=0; j<ROUTING_TABLE_COLUMNS; j++)
                ui->routingTable->setItem(i,j, new QTableWidgetItem());
        }
    }

    // refill table
    for(qint32 i=0; i<routingInfo.size(); i++)
    {
        // if amount of packs haven't changed, skip update
        if(ui->routingTable->item(i, 0)->text() == routingInfo[i].srcIP &&
           ui->routingTable->item(i, 1)->text() == routingInfo[i].dstIP &&
           ui->routingTable->item(i, 2)->text() == QString::number(routingInfo[i].totalAmount)) continue;

        ui->routingTable->item(i, 0)->setText(routingInfo[i].srcIP);
        ui->routingTable->item(i, 1)->setText(routingInfo[i].dstIP);
        ui->routingTable->item(i, 2)->setText(QString::number(routingInfo[i].totalAmount));
        if(routingInfo[i].totalSize > GB)
            ui->routingTable->item(i,3)->setText(QString::number(routingInfo[i].totalSize/float(GB)) + " GB");
        else if(routingInfo[i].totalSize > MB)
            ui->routingTable->item(i,3)->setText(QString::number(routingInfo[i].totalSize/float(MB)) + " MB");
        else if(routingInfo[i].totalSize > KB)
            ui->routingTable->item(i,3)->setText(QString::number(routingInfo[i].totalSize/float(KB)) + " KB");
        else
            ui->routingTable->item(i,3)->setText(QString::number(routingInfo[i].totalSize) + " B");
    }
}

void interfaceManager::sortRoutingTable(int logicalIndex)
{
    m_data->setRoutingTableSortColumn(logicalIndex);
}

void interfaceManager::INswitched(bool f)
{
    m_data->manageHooks(IN, f);
}

void interfaceManager::OUTswitched(bool f)
{
    m_data->manageHooks(OUT, f);
}

void interfaceManager::FWDswitched(bool f)
{
    m_data->manageHooks(FWD, f);
}

void interfaceManager::switchLines()
{
    bool opt[6];
    opt[0] = ui->checkBoxChartIN->isChecked();
    opt[1] = ui->checkBoxChartFWD->isChecked();
    opt[2] = ui->checkBoxChartOUT->isChecked();
    opt[3] = ui->checkBoxChartINsize->isChecked();
    opt[4] = ui->checkBoxChartFWDsize->isChecked();
    opt[5] = ui->checkBoxChartOUTsize->isChecked();
    m_data->switchLines(opt);
}

void interfaceManager::saveStats()
{
    QString fName = QDate::currentDate().toString("dd_MM_yyyy_") + QTime::currentTime().toString() + ".stats";
    QFile file(fName);
    file.open(QIODevice::WriteOnly);
    QTextStream stream(&file);

    m_data->saveStats(stream);

    file.close();

    QMessageBox::information(this, "Info", "Stats have been saved: " + fName);
}

void interfaceManager::loadStats()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                       "Open", "", "Image Files (*.stats)");

    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly)) return;
    QTextStream stream(&file);

    ui->routingTable->setRowCount(0);
    m_data->loadStats(stream);

    file.close();
}
