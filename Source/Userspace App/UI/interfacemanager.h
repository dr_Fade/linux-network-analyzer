#ifndef INTERFACEMANAGER_H
#define INTERFACEMANAGER_H

#include <QMainWindow>
#include "../Data_Manager/datamanager.h"

namespace Ui {
class interfaceManager;
}

class interfaceManager : public QMainWindow
{
    Q_OBJECT
    dataManager *m_data;
    QChartView  *m_chartView;

public:
    explicit interfaceManager(QWidget *parent = 0);
    ~interfaceManager();

public slots:
    void scaleChart();
    void updateTables(infoVector generalInfo, routingInfoVector routingInfo);

    void sortRoutingTable(int logicalIndex);

    void INswitched(bool f);
    void OUTswitched(bool f);
    void FWDswitched(bool f);

    void switchLines();

    void saveStats();
    void loadStats();

signals:
    void hookChanged(Hook hook, bool action);

private:
    Ui::interfaceManager *ui;
};

#endif // INTERFACEMANAGER_H
