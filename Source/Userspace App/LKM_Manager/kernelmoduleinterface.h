#ifndef KERNELMODULEINTERFACE_H
#define KERNELMODULEINTERFACE_H

#include <fcntl.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <syscall.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include "misc.h"

class kernelModuleInterface: public QObject
{
    Q_OBJECT
    infoVector       m_chartStats,
                     m_generalStats,
                     m_routingStats;
    QTimer          *m_timerPoll,
                    *m_timerEmit;
    qint32           m_devFD;
    quint32          m_bytesRead,
                     m_structsRead;
    quint16          m_revents;

    struct pollfd                   m_pfd;
    QVector<struct nm_packet_info>  m_buf;

public:
    kernelModuleInterface(QObject *parent = nullptr);
    ~kernelModuleInterface();

signals:
    void sendInfo(infoVector chartStats,
                  infoVector totalStats,
                  infoVector routingStats);
    void finished();

private slots:
    void emitData();

public slots:
    void ioctlTrigger(HooksIOCTL ctl);
    void quit();
    void queryLKM();

};

#endif // KERNELMODULEINTERFACE_H
