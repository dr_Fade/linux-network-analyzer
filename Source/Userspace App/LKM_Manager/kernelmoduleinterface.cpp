#include "kernelmoduleinterface.h"

kernelModuleInterface::kernelModuleInterface(QObject *parent):
    QObject(parent),
    m_devFD(0)
{
    // loading LKM
    system("insmod ./netmon.ko");

    m_buf.resize(sizeof(struct nm_packet_info)*BUF_SIZE);
    m_devFD = open("/dev/netmon", O_RDONLY | O_NONBLOCK);

    m_pfd.fd = m_devFD;
    m_pfd.events = POLLIN;

    // reserving memory for chart and total info
    m_chartStats.fill(0, LINES_ON_CHART);
    m_generalStats.fill(0, GEN_TABLE_SIZE);
    m_routingStats.clear();

    m_timerPoll = new QTimer(this);
    m_timerEmit = new QTimer(this);
    connect(m_timerPoll, &QTimer::timeout,
            this,    &kernelModuleInterface::queryLKM);
    connect(m_timerEmit, &QTimer::timeout,
            this,    &kernelModuleInterface::emitData);
    m_timerPoll->start();
    m_timerEmit->start();
}

kernelModuleInterface::~kernelModuleInterface()
{
    close(m_devFD);
    system("rmmod netmon");
}

void kernelModuleInterface::emitData()
{
    // B => KB
    for(int i = LINES_ON_CHART/2; i<LINES_ON_CHART; i++)
        if(m_chartStats[i-3]) m_chartStats[i] /= m_chartStats[i-3];

    emit sendInfo(m_chartStats, m_generalStats, m_routingStats);
    m_chartStats.fill(0, LINES_ON_CHART);
    m_generalStats.fill(0, GEN_TABLE_SIZE);
    m_routingStats.clear();
    m_timerEmit->start(1000);
}

void kernelModuleInterface::ioctlTrigger(HooksIOCTL ctl)
{
    switch (ctl)
    {
    case IN_UP:
        ioctl(m_devFD, NM_IOCTL_IN_UP);
        break;
    case IN_DOWN:
        ioctl(m_devFD, NM_IOCTL_IN_DOWN);
        break;
    case OUT_UP:
        ioctl(m_devFD, NM_IOCTL_OUT_UP);
        break;
    case OUT_DOWN:
        ioctl(m_devFD, NM_IOCTL_OUT_DOWN);
        break;
    case FWD_UP:
        ioctl(m_devFD, NM_IOCTL_FWD_UP);
        break;
    case FWD_DOWN:
        ioctl(m_devFD, NM_IOCTL_FWD_DOWN);
        break;
    }
}

void kernelModuleInterface::quit()
{
    disconnect(m_timerPoll, &QTimer::timeout,
               this,    &kernelModuleInterface::queryLKM);
    disconnect(m_timerEmit, &QTimer::timeout,
               this,    &kernelModuleInterface::emitData);
    emit finished(); // to finish lkmThread
}

void kernelModuleInterface::queryLKM()
{
    if (poll(&m_pfd, 1, 0) == -1) exit(EXIT_FAILURE);

    m_revents = m_pfd.revents;
    if (m_revents & POLLIN)
    {
        m_bytesRead = read(m_pfd.fd, m_buf.data(), sizeof(struct nm_packet_info)*BUF_SIZE);
        m_structsRead = m_bytesRead/sizeof(struct nm_packet_info);
        for (quint32 i = 0; i < m_structsRead; ++i)
        {
            // pack info for general stats table
            switch (m_buf[i].proto)
            {
            case 1:
                m_generalStats[ICMP + m_buf[i].hook - 1]++; // -1 cuz IN hook is '1'
                break;
            case 6:
                m_generalStats[TCP + m_buf[i].hook - 1]++;
                break;
            case 17:
                m_generalStats[UDP + m_buf[i].hook - 1]++;
                break;
            default:
                m_generalStats[OTHER + m_buf[i].hook - 1]++;
            }
            m_generalStats[11 + m_buf[i].hook] += m_buf[i].len;
            // pack info for routing table
            m_routingStats.push_back(m_buf[i].src_ip);
            m_routingStats.push_back(m_buf[i].dst_ip);
            m_routingStats.push_back(1);
            m_routingStats.push_back(m_buf[i].len);
            // pack info for chart
            m_chartStats[m_buf[i].hook - 1]++; // amount
            m_chartStats[m_buf[i].hook + 2] += m_buf[i].len; // size
        }
    }
    m_timerPoll->start(1);
}
