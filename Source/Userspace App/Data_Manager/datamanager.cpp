#include "datamanager.h"

dataManager::dataManager(QObject *parent):
    QObject(parent),
    m_currTime(0),
    m_timeAxisDisp(20),
    m_routingSortColumn(2),
    m_sortRoutingTableBySrc(Qt::DescendingOrder),
    m_sortRoutingTableByDst(Qt::DescendingOrder),
    m_sortRoutingTableByAmount(Qt::DescendingOrder),
    m_sortRoutingTableBySize(Qt::DescendingOrder)
{
    //configure lkm listener
    m_lkmListener = new kernelModuleInterface(this);

    connect(m_lkmListener,  &kernelModuleInterface::sendInfo,
            this,           &dataManager::updateDataModels);

    //setting up chart
    m_chart       = new QChart;
    m_timeAxis    = new QValueAxis(m_chart);
    m_amountAxis  = new QValueAxis(m_chart);
    m_sizeAxis    = new QValueAxis(m_chart);

    for(qint32 i=0; i<LINES_ON_CHART; i++)
    {
        m_lineSeries.push_back(new QLineSeries());
        m_chart->addSeries(m_lineSeries[i]);
    }

    m_timeAxis->setTitleText("Time, s");
    m_amountAxis->setTitleText("Amount");
    m_sizeAxis->setTitleText("Size, B");

    m_chart->addAxis(m_timeAxis, Qt::AlignBottom);
    m_chart->addAxis(m_amountAxis, Qt::AlignLeft);
    m_chart->addAxis(m_sizeAxis, Qt::AlignRight);

    m_lineSeries[0]->setName("IN");
    m_lineSeries[0]->setColor(Qt::red);

    m_lineSeries[1]->setName("FWD");
    m_lineSeries[1]->setColor(Qt::green);

    m_lineSeries[2]->setName("OUT");
    m_lineSeries[2]->setColor(Qt::blue);

    m_lineSeries[3]->setName("AVG IN size");
    m_lineSeries[4]->setName("AVG FWD size");
    m_lineSeries[5]->setName("AVG OUT size");

    for(qint32 i=0; i<LINES_ON_CHART; i++)
    {
        m_lineSeries[i]->attachAxis(m_timeAxis);
        if(i<LINES_ON_CHART/2)
            m_lineSeries[i]->attachAxis(m_amountAxis);
        else
        {
            m_lineSeries[i]->setPen(Qt::DashLine);
            m_lineSeries[i]->attachAxis(m_sizeAxis);
        }
    }
    m_lineSeries[3]->setColor(Qt::red);
    m_lineSeries[4]->setColor(Qt::green);
    m_lineSeries[5]->setColor(Qt::blue);

    m_generalInfo.fill(0, GEN_TABLE_SIZE);
}

dataManager::~dataManager()
{
    m_lkmListener->quit();
    for(auto &t : m_lineSeries) delete t;
}

QChart *dataManager::getChart()
{
    return m_chart;
}

void dataManager::scaleTimeAxis(qint32 timeDisp)
{
    m_timeAxis->setMax(m_currTime);
    if(m_timeAxisDisp != timeDisp)
    {
        m_timeAxisDisp = timeDisp;
        m_timeAxis->setMin(m_currTime - m_timeAxisDisp);
    }
}

void dataManager::saveStats(QTextStream &stream)
{
    for(auto &i : m_generalInfo)
        stream << i << ' ';

    stream << '\n' << m_routingInfo.size() << '\n';

    for(routingTableItem& i : m_routingInfo)
    {
        stream << i.srcIP << ' '
               << i.dstIP << ' '
               << i.totalAmount << ' '
               << i.totalSize << '\n';
    }
}

void dataManager::loadStats(QTextStream &stream)
{
    m_generalInfo.clear();
    m_routingInfo.clear();
    quint32 tmp1;
    for(qint32 i=0; i<GEN_TABLE_SIZE; i++)
    {
        stream >> tmp1;
        m_generalInfo.push_back(tmp1);
    }

    quint32 routingSize;
    routingTableItem tmp2;
    stream >> routingSize;
    for(quint32 i=0; i<routingSize; i++)
    {
        stream >> tmp2.srcIP >> tmp2.dstIP >> tmp2.totalAmount >> tmp2.totalSize;
        m_routingInfo.push_back(tmp2);
    }
}

void dataManager::switchLines(bool opt[])
{
    for(int i=0; i<LINES_ON_CHART; i++)
        if(opt[i])
            m_lineSeries[i]->show();
        else
            m_lineSeries[i]->hide();
}

void dataManager::setRoutingTableSortColumn(qint32 column)
{
    if(column < 0 || column > 3) return;

    m_routingSortColumn = column;
    switch (m_routingSortColumn)
    {
    case 0:
        m_sortRoutingTableBySrc = (m_sortRoutingTableBySrc == Qt::AscendingOrder)?Qt::DescendingOrder : Qt::AscendingOrder;
        sortRoutingTableBySrc();
        break;
    case 1:
        m_sortRoutingTableByDst = (m_sortRoutingTableByDst == Qt::AscendingOrder)?Qt::DescendingOrder : Qt::AscendingOrder;
        sortRoutingTableByDst();
        break;
    case 2:
        m_sortRoutingTableByAmount = (m_sortRoutingTableByAmount == Qt::AscendingOrder)?Qt::DescendingOrder : Qt::AscendingOrder;
        sortRoutingTableByAmount();
        break;
    case 3:
        m_sortRoutingTableBySize = (m_sortRoutingTableBySize == Qt::AscendingOrder)?Qt::DescendingOrder : Qt::AscendingOrder;
        sortRoutingTableBySize();
        break;
    }
    emit tablesUpdated(m_generalInfo, m_routingInfo);
}

void dataManager::sortRoutingTableBySrc()
{
    if (m_sortRoutingTableBySrc == Qt::AscendingOrder)
    {
        auto lessThan = [](routingTableItem &i1, routingTableItem &i2) {
            return i1.srcIP < i2.srcIP;
        };
        std::sort(m_routingInfo.begin(), m_routingInfo.end(), lessThan);
    }
    else
    {
        auto greaterThan = [](routingTableItem &i1, routingTableItem &i2) {
            return i1.srcIP > i2.srcIP;
        };
        std::sort(m_routingInfo.begin(), m_routingInfo.end(), greaterThan);
    }
}

void dataManager::sortRoutingTableByDst()
{
    if (m_sortRoutingTableByDst == Qt::AscendingOrder)
    {
        auto lessThan = [](routingTableItem &i1, routingTableItem &i2) {
            return i1.dstIP < i2.dstIP;
        };
        std::sort(m_routingInfo.begin(), m_routingInfo.end(), lessThan);
    }
    else
    {
        auto greaterThan = [](routingTableItem &i1, routingTableItem &i2) {
            return i1.dstIP > i2.dstIP;
        };
        std::sort(m_routingInfo.begin(), m_routingInfo.end(), greaterThan);
    }
}

void dataManager::sortRoutingTableByAmount()
{
    if (m_sortRoutingTableByAmount == Qt::AscendingOrder)
    {
        auto lessThan = [](routingTableItem &i1, routingTableItem &i2) {
            return i1.totalAmount < i2.totalAmount;
        };
        std::sort(m_routingInfo.begin(), m_routingInfo.end(), lessThan);
    }
    else
    {
        auto greaterThan = [](routingTableItem &i1, routingTableItem &i2) {
            return i1.totalAmount > i2.totalAmount;
        };
        std::sort(m_routingInfo.begin(), m_routingInfo.end(), greaterThan);
    }
}

void dataManager::sortRoutingTableBySize()
{
    if (m_sortRoutingTableBySize == Qt::AscendingOrder)
    {
        auto lessThan = [](routingTableItem &i1, routingTableItem &i2) {
            return i1.totalSize < i2.totalSize;
        };
        std::sort(m_routingInfo.begin(), m_routingInfo.end(), lessThan);
    }
    else
    {
        auto greaterThan = [](routingTableItem &i1, routingTableItem &i2) {
            return i1.totalSize > i2.totalSize;
        };
        std::sort(m_routingInfo.begin(), m_routingInfo.end(), greaterThan);
    }
}

void dataManager::updateTablesInfo(infoVector &generalStats, infoVector &routingStats)
{
    for(qint32 i=0; i<GEN_TABLE_SIZE; i++)
        m_generalInfo[i] += generalStats[i];

    for(qint32 i=0; i<routingStats.size(); i+=4)
    {
        // constructing ip strings
        QString srcIP, dstIP;

        srcIP.push_back(QString::number(routingStats[i] & 0xFF));
        srcIP.push_back('.');
        srcIP.push_back(QString::number((routingStats[i] >> 8) & 0xFF));
        srcIP.push_back('.');
        srcIP.push_back(QString::number((routingStats[i] >> 16) & 0xFF));
        srcIP.push_back('.');
        srcIP.push_back(QString::number((routingStats[i] >> 24) & 0xFF));

        dstIP.push_back(QString::number(routingStats[i+1] & 0xFF));
        dstIP.push_back('.');
        dstIP.push_back(QString::number((routingStats[i+1] >> 8) & 0xFF));
        dstIP.push_back('.');
        dstIP.push_back(QString::number((routingStats[i+1] >> 16) & 0xFF));
        dstIP.push_back('.');
        dstIP.push_back(QString::number((routingStats[i+1] >> 24) & 0xFF));

        // insert a unique src-dst pair or update an existing one
        bool insertNew = true;
        for(qint32 j=0; j<m_routingInfo.size(); j++)
        {
            if(m_routingInfo[j].srcIP == srcIP && m_routingInfo[j].dstIP == dstIP)
            {
                insertNew = false;
                m_routingInfo[j].totalAmount += routingStats[i+2];
                m_routingInfo[j].totalSize += routingStats[i+3];
            }
            if(!insertNew) break;
        }
        if(insertNew)
        {
            routingTableItem newItem(srcIP, dstIP,
                                     routingStats[i+2],  // amount
                                     routingStats[i+3]); // size
            m_routingInfo.push_back(newItem);
        }
    }
    switch (m_routingSortColumn)
    {
    case 0:
        sortRoutingTableBySrc();
        break;
    case 1:
        sortRoutingTableByDst();
        break;
    case 2:
        sortRoutingTableByAmount();
        break;
    case 3:
        sortRoutingTableBySize();
        break;
    }
    emit tablesUpdated(m_generalInfo, m_routingInfo);
}

void dataManager::updateLineSeries(infoVector& newStats)
{
    for(int i=0; i<LINES_ON_CHART; i++)
        m_lineSeries[i]->append(m_currTime, newStats[i]);

    // remove points that will never appear on chart
    for(qint32 j=0; j<LINES_ON_CHART; j++)
        if(m_lineSeries[j]->count() > MAX_TIME_RANGE)
            m_lineSeries[j]->removePoints(0, 1);

    m_amountAxisMax = 16;
    m_sizeAxisMax = 8;
    for(int i=0; i<m_lineSeries.size(); i++)
    {
        auto points = m_lineSeries[i]->points();
        if(i < 3)
        {
            for(int j=0; j<m_lineSeries[i]->count(); j++)
            {
                if(points.at(j).y() > m_amountAxisMax)
                    m_amountAxisMax = points.at(j).y();
            }
        }
        else
        {
            for(int j=0; j<m_lineSeries[i]->count(); j++)
                if(points.at(j).y() > m_sizeAxisMax)
                    m_sizeAxisMax = points.at(j).y();
        }
    }

    m_timeAxis->setMax(m_currTime);
    m_timeAxis->setMin(m_currTime - m_timeAxisDisp);
    m_amountAxis->setMax(m_amountAxisMax);
    m_sizeAxis->setMax(m_sizeAxisMax);
}

void dataManager::updateDataModels(infoVector chartStats, infoVector generalStats, infoVector routingStats)
{
    updateLineSeries(chartStats);
    updateTablesInfo(generalStats, routingStats);
    m_currTime++;
}

void dataManager::manageHooks(Hook hook, bool action)
{
    // clearing unused info
    switch (hook)
    {
    case IN:
        if(action)
            m_lkmListener->ioctlTrigger(IN_UP);
        else
            m_lkmListener->ioctlTrigger(IN_DOWN);
        break;

    case OUT:
        if(action)
            m_lkmListener->ioctlTrigger(OUT_UP);
        else
            m_lkmListener->ioctlTrigger(OUT_DOWN);
        break;

    case FWD:
        if(action)
            m_lkmListener->ioctlTrigger(FWD_UP);
        else
            m_lkmListener->ioctlTrigger(FWD_DOWN);
        break;
    }
}
