#ifndef DATA_MANAGER_H
#define DATA_MANAGER_H

#include "../LKM_Manager/kernelmoduleinterface.h"
#include <QtCharts>
#include <QThread>
#include <QtDebug>

class dataManager: public QObject
{
    Q_OBJECT
    kernelModuleInterface   *m_lkmListener;
    // info for tables
    routingInfoVector        m_routingInfo;
    infoVector               m_generalInfo;
    // stuff for charts
    QChart                  *m_chart;
    QValueAxis              *m_timeAxis,
                            *m_amountAxis,
                            *m_sizeAxis;
    QVector<QLineSeries *>   m_lineSeries;
    // controlling range of chart's axes
    qint32                   m_currTime,
                             m_timeAxisDisp,
                             m_amountAxisMax,
                             m_sizeAxisMax,
                             m_routingSortColumn;
    Qt::SortOrder            m_sortRoutingTableBySrc,
                             m_sortRoutingTableByDst,
                             m_sortRoutingTableByAmount,
                             m_sortRoutingTableBySize;

public:
    dataManager(QObject *parent = nullptr);
    ~dataManager();
    QChart *getChart();

    void scaleTimeAxis(qint32 timeDisp);
    void saveStats(QTextStream &stream);
    void loadStats(QTextStream &stream);

    void switchLines(bool opt[]);

    void setRoutingTableSortColumn(qint32 column);
    void sortRoutingTableBySrc();
    void sortRoutingTableByDst();
    void sortRoutingTableByAmount();
    void sortRoutingTableBySize();

    void updateTablesInfo(infoVector& totalStats, infoVector &routingStats);
    void updateLineSeries(infoVector &newStats);

signals:
    void tablesUpdated(infoVector generalInfo, routingInfoVector routingInfo);

public slots:
    void updateDataModels(infoVector chartStats,
                          infoVector generalStats,
                          infoVector routingStats);

    void manageHooks(Hook hook, bool action);
};

#endif
