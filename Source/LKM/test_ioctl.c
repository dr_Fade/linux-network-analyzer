#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include "netmon.h"
 
int main()
{
    int fd = 0;
    int32_t number=1;
    char *char_dev = "/dev/netmon";
    char ifaces[MAX_IFACES][IF_NAME_SIZE];
    int i;

    fd = open(char_dev, O_RDWR);
    if(fd < 0)
    {
        printf("Cannot open device file...\n");
        return 0;
    }
    while (number)
    {
         printf("Enter ioctl (or 7 for exit):\n");
         scanf("%d",&number);
         printf("Call IO # %d\n", number);
         switch (number)
        {
             case 0:
                 ioctl(fd, NM_IOCTL_GET_DEV, ifaces);
                 for (i = 0; i < MAX_IFACES; ++i)
                 {
                     if (ifaces[i][0])
                     {
                         printf("Found interface: %s\n", ifaces[i]);
                     }
                     else break;
                 }
                 break;
             case 1:
                 ioctl(fd, NM_IOCTL_IN_UP);
                 break;
             case 2:
                 ioctl(fd, NM_IOCTL_IN_DOWN);
                 break;
             case 3:
                 ioctl(fd, NM_IOCTL_OUT_UP);
                 break;
             case 4:
                 ioctl(fd, NM_IOCTL_OUT_DOWN);
                 break;
             case 5:
                 ioctl(fd, NM_IOCTL_FWD_UP);
                 break;
             case 6:
                 ioctl(fd, NM_IOCTL_FWD_DOWN);
                 break;
             case 7:
                 close(fd);
                 return 0;
        }
    }
    close(fd);
    return 0;
}
