#include <fcntl.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "netmon.h"

void print_ip(char *direction, int ip)
{
    unsigned char bytes[4];
    bytes[3] = ip & 0xFF;
    bytes[2] = (ip >> 8) & 0xFF;
    bytes[1] = (ip >> 16) & 0xFF;
    bytes[0] = (ip >> 24) & 0xFF;   
    printf("%s: %d.%d.%d.%d\n", direction, bytes[3], bytes[2], bytes[1], bytes[0]);        
}

int main(void)
{
    char *char_dev = "/dev/netmon";
    int fd = 0;
    int i = 0;
    int n = 0;
    short revents = 0;
    size_t buf_size = 200;

    struct pollfd pfd;
    struct nm_packet_info *buf = malloc(sizeof(struct nm_packet_info)*buf_size);

    fd = open(char_dev, O_RDONLY | O_NONBLOCK);
    if (fd == -1)
    {
        perror("open");
        exit(EXIT_FAILURE);
    }
    pfd.fd = fd;
    pfd.events = POLLIN;
    while (1)
    {
        i = poll(&pfd, 1, -1);
        if (i == -1)
        {
            perror("poll");
            exit(EXIT_FAILURE);
        }
        revents = pfd.revents;
        if (revents & POLLIN)
        {
            n = read(pfd.fd, buf, sizeof(struct nm_packet_info)*buf_size);
            for (i = 0; i < n/sizeof(struct nm_packet_info); ++i)
            {
                printf("--------------------------------\n");
                printf("Hook: %d, proto %d, len: %d, if_in: %s, if_out: %s\n", buf[i].hook, buf[i].proto, buf[i].len, buf[i].if_in, buf[i].if_out);
                print_ip("SRC", buf[i].src_ip);
                print_ip("DST", buf[i].dst_ip);
            }
        }
    }
    close(fd);
    return 0;
}
