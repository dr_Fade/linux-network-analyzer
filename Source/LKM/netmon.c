#include <linux/init.h>           // Macros used to mark up functions e.g. __init __exit
#include <linux/module.h>         // Core header for loading LKMs into the kernel
#include <linux/device.h>         // Header to support the kernel Driver Model
#include <linux/kernel.h>         // Contains types, macros, functions for the kernel
#include <linux/fs.h>             // Header for the Linux file system support
#include <linux/uaccess.h>          // Required for the copy to user function
#include <linux/slab.h>
#include <linux/ioctl.h>
#include <linux/wait.h>
#include <linux/poll.h>
#include <linux/mutex.h>
#include <linux/spinlock.h>
#include <linux/printk.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/netfilter_bridge.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/udp.h>
#include <linux/tcp.h>
#include <linux/ip.h>
#include <linux/icmp.h>
#include <net/ip.h>

#include "netmon.h"


MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("A simple Linux network monitor driver");
MODULE_VERSION("0.1");


#define DEVICE_NAME "netmon"    // Character device name at /dev
#define CLASS_NAME "netmon"
#define DEBUG

#define ETH_HEADER_SIZE 14
#define HOOK_STATUS_LEN 4


/* Char device variables */

static int major_number;
static struct class* netmon_class = NULL;
static struct device* netmon_device = NULL;

/* Netfilter variables */

static struct nf_hook_ops nfho_in;
static struct nf_hook_ops nfho_out;
static struct nf_hook_ops nfho_fwd;

static struct iphdr *ip_header;
static struct udphdr *udp_header;
static struct tcphdr *tcp_header;
static struct icmphdr *icmp_header;

/* Netmon buffer and hook variables */

static struct nm_packet_info *packet_buf = NULL;
static struct nm_packet_info *cur_buf_pos = NULL;
static int buf_changed = 0;
static int hook_status[HOOK_STATUS_LEN] = {0};
static DECLARE_WAIT_QUEUE_HEAD(nm_poll_wait);

/* Buffer and hook synchronization variables */

static spinlock_t buf_lock = __SPIN_LOCK_UNLOCKED();
static spinlock_t hook_status_lock = __SPIN_LOCK_UNLOCKED();
static unsigned long hook_status_flags;
static unsigned long buf_flags;

/* Char device functions prototypes */

static ssize_t nm_read(struct file *, char *, size_t, loff_t *);
unsigned int nm_poll(struct file *, poll_table *);
static long nm_ioctl(struct file *, unsigned int, unsigned long);

static struct file_operations fops = {
    .read = nm_read,
    .poll = nm_poll,
    .unlocked_ioctl = nm_ioctl
};


static unsigned int hook_func(void *priv, struct sk_buff *skb, const struct nf_hook_state *state)
{
    spin_lock_irqsave(&hook_status_lock, hook_status_flags);

    /* Check if socket buffer is valid. Ignore if the packet is invalid */

    if (!hook_status[state->hook] || !skb)
    {
        spin_unlock_irqrestore(&hook_status_lock, hook_status_flags);
        return NF_ACCEPT;
    }
    spin_unlock_irqrestore(&hook_status_lock, hook_status_flags);

    ip_header = (struct iphdr *)skb_network_header(skb);

    spin_lock_irqsave(&buf_lock, buf_flags);

    cur_buf_pos->hook = state->hook;
    cur_buf_pos->src_ip = ip_header->saddr;
    cur_buf_pos->dst_ip = ip_header->daddr;
    cur_buf_pos->proto = ip_header->protocol;
    cur_buf_pos->len = skb->len + ETH_HEADER_SIZE;

    if (state->in->name)
    {
        strncpy(cur_buf_pos->if_in, state->in->name, IF_NAME_SIZE);
    }
    else
    {
        cur_buf_pos->if_in[0] = '\0';
    }
    if (state->out->name)
    {
        strncpy(cur_buf_pos->if_out, state->out->name, IF_NAME_SIZE);
    }
    else
    {
        cur_buf_pos->if_out[0] = '\0';
    }

    /* Reset current buffer position to start if buffer is full */

    if (cur_buf_pos == &packet_buf[BUF_SIZE - 1])
    {
        cur_buf_pos = packet_buf;
    }
    else
    {
        cur_buf_pos++;
    }
    buf_changed = 1;

    spin_unlock_irqrestore(&buf_lock, buf_flags);
    wake_up_interruptible(&nm_poll_wait);

#ifdef DEBUG

    pr_info("---------------------------------------------------\n");
    pr_info("Hook: %u, proto: %d, len: %d, if_in: %s, if_out: %s\n", state->hook, ip_header->protocol, skb->len + ETH_HEADER_SIZE, state->in->name, state->out->name);
    switch (ip_header->protocol)
    {
        case IPPROTO_UDP:
            udp_header = (struct udphdr *)(skb_transport_header(skb) + ip_hdrlen(skb));
            if (udp_header) {
                pr_info("SRC: %pI4:%d\nDST: %pI4:%d\n",
                        &ip_header->saddr,
                        ntohs(udp_header->source),
                        &ip_header->daddr,
                        ntohs(udp_header->dest)
                       );
            }
            pr_info("*** UDP ***\n");
            break;

        case IPPROTO_TCP:
            tcp_header = (struct tcphdr *)(skb_transport_header(skb) + ip_hdrlen(skb));
            if (tcp_header) {
                pr_info("SRC: (%pI4):%d\nDST: (%pI4):%d\n",
                        &ip_header->saddr,
                        ntohs(tcp_header->source),
                        &ip_header->daddr,
                        ntohs(tcp_header->dest)
                       );
            }
            pr_info("*** TCP ***\n");
            break;

        case IPPROTO_ICMP:
            icmp_header = (struct icmphdr *)(skb_transport_header(skb) + ip_hdrlen(skb));
            if (icmp_header) {
                pr_info("SRC: (%pI4)\nDST: (%pI4)\n",
                        &ip_header->saddr,
                        &ip_header->daddr
                        );
                pr_info("ICMP type: %d - ICMP code: %d\n",
                        icmp_header->type,
                        icmp_header->code
                        );
            }
            pr_info("*** ICMP ***\n");
            break;
    }
#endif //DEBUG

    return NF_ACCEPT;
}


static int register_hook(unsigned int hooknum, struct nf_hook_ops *nfho)
{
    int reg_res = 0;

    spin_lock_irqsave(&hook_status_lock, hook_status_flags);

    if (!hook_status[hooknum])
    {
        nfho->hook = hook_func;
        nfho->hooknum = hooknum;
        nfho->pf = PF_INET;
        nfho->priority = NF_IP_PRI_FIRST;
        nfho->dev = first_net_device(&init_net);
        reg_res = nf_register_net_hook(&init_net, nfho);
        if (reg_res)
        {
            printk(KERN_ERR "Netmon: Unable to register Netfilter hook %d, error: %d\n", hooknum, reg_res);
            hook_status[hooknum] = 0;
            spin_unlock_irqrestore(&hook_status_lock, hook_status_flags);
            return reg_res;
        };
        hook_status[hooknum] = 1;

#ifdef DEBUG
        printk(KERN_INFO "Netmon: Netfilter hook %d has been registered\n", hooknum);
#endif
    }
    spin_unlock_irqrestore(&hook_status_lock, hook_status_flags);
    return 0;
}

void unregister_hook(unsigned int hooknum, struct nf_hook_ops *nfho)
{
    spin_lock_irqsave(&hook_status_lock, hook_status_flags);

    if (hook_status[hooknum])
    {
        nf_unregister_net_hook(&init_net, nfho);
        hook_status[hooknum] = 0;

#ifdef DEBUG
        printk(KERN_INFO "Netmon: Netfilter hook %d has been unregistered\n", hooknum);
#endif
    }

    spin_unlock_irqrestore(&hook_status_lock, hook_status_flags);
}

static int get_net_devices(unsigned long arg)
{
    struct net_device *dev;
    int if_count = 0;
    char if_names_buf[MAX_IFACES][IF_NAME_SIZE] = {'\0'};
    int buf_len = MAX_IFACES * IF_NAME_SIZE;

    read_lock(&dev_base_lock);
    dev = first_net_device(&init_net);
    while (dev)
    {
        strcpy(if_names_buf[if_count], dev->name);
        dev = next_net_device(dev);
        ++if_count;
    }
    read_unlock(&dev_base_lock);

    if (copy_to_user((void *) arg, if_names_buf, buf_len))
    {
        printk(KERN_INFO "Netmon: Failed to send %d bytes to the user\n", buf_len);
        return -EFAULT;
    }
    return 0;
}


static long nm_ioctl(struct file *file, unsigned int ioctl_num, unsigned long arg)
{
    switch (ioctl_num) {
        case NM_IOCTL_GET_DEV:
            get_net_devices(arg);
            break;

        case NM_IOCTL_IN_UP:
            register_hook(NF_BR_LOCAL_IN, &nfho_in);
            break;

        case NM_IOCTL_IN_DOWN:
            unregister_hook(NF_BR_LOCAL_IN, &nfho_in);
            break;

        case NM_IOCTL_OUT_UP:
            register_hook(NF_BR_LOCAL_OUT, &nfho_out);
            break;

        case NM_IOCTL_OUT_DOWN:
            unregister_hook(NF_BR_LOCAL_OUT, &nfho_out);
            break;

        case NM_IOCTL_FWD_UP:
            register_hook(NF_BR_FORWARD, &nfho_fwd);
            break;

        case NM_IOCTL_FWD_DOWN:
            unregister_hook(NF_BR_FORWARD, &nfho_fwd);
    }
    return 0;
}


static int init_netfilter(void)
{
    // NF_BR_PRE_ROUTING 0
    // NF_BR_LOCAL_IN    1
    // NF_BR_FORWARD     2
    // NF_BR_LOCAL_OUT   3

    packet_buf = kmalloc(BUF_SIZE * sizeof(struct nm_packet_info), GFP_KERNEL);
    if (!packet_buf)
    {
        printk(KERN_INFO "Unable to allocate memory for netfilter buffer\n");
        return -ENOMEM;
    }
    cur_buf_pos = packet_buf;

    register_hook(NF_BR_LOCAL_IN, &nfho_in);
    register_hook(NF_BR_FORWARD, &nfho_fwd);
    register_hook(NF_BR_LOCAL_OUT, &nfho_out);

    return 0;
}


void exit_netfilter(void)
{
    unregister_hook(NF_BR_LOCAL_IN, &nfho_in);
    unregister_hook(NF_BR_FORWARD, &nfho_fwd);
    unregister_hook(NF_BR_LOCAL_OUT, &nfho_out);
    kfree(packet_buf);
}


static int __init netmon_init(void){
    int nf_res;

    printk(KERN_INFO "Netmon: Initializing netmon\n");

    nf_res = init_netfilter();
    if (nf_res)
    {
        return -1;
    }

    /* Try to dynamically allocate a major number for the device */

    major_number = register_chrdev(0, DEVICE_NAME, &fops);
    if (major_number<0)
    {
        printk(KERN_ALERT "Netmon failed to register a major number\n");
        return major_number;
    }
    printk(KERN_INFO "Netmon: registered correctly with major number %d\n", major_number);

    /* Register the device class */

    netmon_class = class_create(THIS_MODULE, CLASS_NAME);
    if (IS_ERR(netmon_class))
    {
        unregister_chrdev(major_number, DEVICE_NAME);
        printk(KERN_ALERT "Failed to register device class\n");
        return PTR_ERR(netmon_class);
    }
    printk(KERN_INFO "Netmon: device class registered correctly\n");

    /* Register the device driver */

    netmon_device = device_create(netmon_class, NULL, MKDEV(major_number, 0), NULL, DEVICE_NAME);
    if (IS_ERR(netmon_device))
    { 
        class_destroy(netmon_class);
        unregister_chrdev(major_number, DEVICE_NAME);
        printk(KERN_ALERT "Failed to create the device\n");
        return PTR_ERR(netmon_device);
    }
    printk(KERN_INFO "Netmon: device class created correctly\n");

    return 0;
} 


static void __exit netmon_exit(void)
{
    exit_netfilter();
    device_destroy(netmon_class, MKDEV(major_number, 0));
    class_unregister(netmon_class);
    class_destroy(netmon_class);
    unregister_chrdev(major_number, DEVICE_NAME);
    printk(KERN_INFO "Netmon: LKM unloaded\n");
}


static ssize_t nm_read(struct file *filep, char *buffer, size_t len, loff_t *offset)
{
    int copy_status = 0;
    int readbuf_len = 0;

    if (buf_changed)
    {
        spin_lock_irqsave(&buf_lock, buf_flags);

        readbuf_len = (cur_buf_pos - packet_buf) * sizeof(struct nm_packet_info);
        copy_status = copy_to_user(buffer, packet_buf, readbuf_len);

        cur_buf_pos = packet_buf;
        buf_changed = 0;

        spin_unlock_irqrestore(&buf_lock, buf_flags);

        if (copy_status)
        {
            printk(KERN_INFO "Netmon: Failed to send %d bytes to the user\n", readbuf_len);
            return -EFAULT;
        }
    }

    return readbuf_len;
}


unsigned int nm_poll(struct file *filep, poll_table *wait)
{
    poll_wait(filep, &nm_poll_wait, wait);
    if (buf_changed)
    {
        return POLLIN | POLLRDNORM;
    }
    return 0;
}


module_init(netmon_init);
module_exit(netmon_exit);
