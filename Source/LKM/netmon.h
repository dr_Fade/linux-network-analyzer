#ifndef NM_H_FLAG
#define NM_H_FLAG

#define IF_NAME_SIZE 16
#define MAX_IFACES 5

#define NM_IOCTL_GET_DEV  _IOWR('k', 0, char [MAX_IFACES][IF_NAME_SIZE])
#define NM_IOCTL_IN_UP    _IO('k', 1)
#define NM_IOCTL_IN_DOWN  _IO('k', 2)
#define NM_IOCTL_OUT_UP   _IO('k', 3)
#define NM_IOCTL_OUT_DOWN _IO('k', 4)
#define NM_IOCTL_FWD_UP   _IO('k', 5)
#define NM_IOCTL_FWD_DOWN _IO('k', 6)

#define BUF_SIZE 2048    // Number of nm_packet_info structs in a buffer

struct __attribute__((__packed__)) nm_packet_info {
    unsigned int hook;
    unsigned int src_ip;
    unsigned int dst_ip;
    unsigned char proto;
    unsigned int len;
    unsigned int length;
    char if_in[IF_NAME_SIZE];
    char if_out[IF_NAME_SIZE];
};

#endif
