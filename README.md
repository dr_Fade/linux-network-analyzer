# Linux Network Analyzer
Application uses the 'netmon' LKM to measure network traffic on local machine and display gathered statistics on chart and tables.
It requires 5.11< version of Qt and 'qtcharts-devel'.

** Build the userspace application (Fedora 28): **

Method 1: 

Go to '/linux-network-analyzer/Source/Userspace\ App/' and execute the following commands:
`$ qmake-qt5 linux-network-analyzer.pro`
`$ make`

Method 2:

Go to '/linux-network-analyzer/Source/Userspace\ App/', open the linux-network-analyzer.pro with QtCreator and build it from IDE.

** Build the driver: **

Go to 'linux-network-analyzer/Source/LKM/' and execute:
`$ make`

** To start the application: ** 

Put 'netmon.ko' into application's directory and launch linux-network-analyzer with root privileges.